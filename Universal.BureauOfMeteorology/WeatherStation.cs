﻿using Universal.Common.Data;

namespace Universal.BureauOfMeteorology
{
    /// <summary>
    /// Represents data about a BOM station.
    /// </summary>
    public class WeatherStation
    {
        [FixedWidthRange(0, 6)]
        public string Site { get; set; }
        [FixedWidthRange(8, 12)]
        public string District { get; set; }
        [FixedWidthRange(14, 53)]
        public string SiteName { get; set; }
        [FixedWidthRange(55, 61)]
        public int StartYear { get; set; }
        [FixedWidthRange(63, 69)]
        public int? EndYear { get; set; }
        [FixedWidthRange(71, 78)]
        public double Latitude { get; set; }
        [FixedWidthRange(80, 88)]
        public double Longitude { get; set; }
        [FixedWidthRange(90, 103)]
        public string Source { get; set; }
        [FixedWidthRange(105, 107)]
        public string State { get; set; }
        [FixedWidthRange(109, 118)]
        public double? Height { get; set; }
        [FixedWidthRange(120, 127)]
        public double? BarometricHeight { get; set; }
        [FixedWidthRange(129, 134)]
        public string WmoReference { get; set; }
    }
}
