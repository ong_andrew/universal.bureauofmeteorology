﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Universal.Common.Collections;
using Universal.Common.Reflection;
using Universal.Common.Serialization;

namespace Universal.BureauOfMeteorology
{
    [JsonObject]
    public class LatestWeatherObservationsResponse : JsonSerializable<LatestWeatherObservationsResponse>, IEnumerable<LatestWeatherObservationsResponse.WeatherObservation>
    {
        [JsonProperty("observations")]
        public ObservationsInfo Observations { get; set; }

        public IEnumerator<WeatherObservation> GetEnumerator()
        {
            return Observations.Data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public class ObservationsInfo
        {
            [JsonProperty("notice")]
            public List<NoticeInfo> Notice { get; set; }
            [JsonProperty("header")]
            public List<HeaderInfo> Header { get; set; }
            [JsonProperty("data")]
            public List<WeatherObservation> Data { get; set; }

            public ObservationsInfo()
            {
                Notice = new List<NoticeInfo>();
                Header = new List<HeaderInfo>();
                Data = new List<WeatherObservation>();
            }
        }


        public class NoticeInfo
        {
            [JsonProperty("copyright")]
            public string Copyright { get; set; }
            [JsonProperty("copyright_url")]
            public string CopyrightUrl { get; set; }
            [JsonProperty("disclaimer_url")]
            public string disclaimerUrl { get; set; }
            [JsonProperty("feedback_url")]
            public string FeedbackUrl { get; set; }
        }


        public class HeaderInfo
        {
            [JsonProperty("refresh_message")]
            public string RefreshMessage { get; set; }
            [JsonProperty("ID")]
            public string Id { get; set; }
            [JsonProperty("main_ID")]
            public string MainId { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("state_time_zone")]
            public string StateTimeZone { get; set; }
            [JsonProperty("time_zone")]
            public string TimeZone { get; set; }
            [JsonProperty("product_name")]
            public string ProductName { get; set; }
            [JsonProperty("state")]
            public string State { get; set; }
        }

        public class WeatherObservation : SparseDictionary<string, object>
        {
            public int SortOrder { get => (int)this["sort_order"]; set => this["sort_order"] = value; }
            public string Wmo { get => this["wmo"] as string; set => this["wmo"] = value; }
            public string Name { get => this["name"] as string; set => this["name"] = value; }
            public string HistoryProduct { get => this["history_product"] as string; set => this["history_product"] = value; }
            public DateTime LocalDateTime
            {
                get
                {
                    string localDateTimeFull = this["local_date_time_full"] as string;
                    return DateTime.ParseExact(localDateTimeFull, "yyyyMMddHHmmss", CultureInfo.CurrentCulture);
                }
            }
            public double Latitude { get => (float)this["lat"]; }
            public double Longitude { get => (float)this["lon"]; }
            /// <summary>
            /// Steadman Apparent Temperature (°C).
            /// </summary>
            public double? ApparentTemperature 
            { 
                get
                {
                    if (this["apparent_t"] != null)
                    {
                        if (this["apparent_t"].GetType().IsCastableTo(typeof(double)))
                        {
                            return (double)this["apparent_t"];
                        }
                    }

                    return null;
                }
            }
            /// <summary>
            /// Ambient temperature (°C).
            /// </summary>
            public double? AirTemperature
            {
                get
                {
                    if (this["air_temp"] != null)
                    {
                        if (this["air_temp"].GetType().IsCastableTo(typeof(double)))
                        {
                            return (double)this["air_temp"];
                        }
                    }

                    return null;
                }
            }
            /// <summary>
            /// Rain since 9am (mm).
            /// </summary>
            public double? RainTrace
            {
                get
                {
                    if (this["rain_trace"] is string rainTraceString)
                    {
                        if (double.TryParse(rainTraceString, out double rainTrace))
                        {
                            return rainTrace;
                        }
                    }

                    return null;
                }
            }
            /// <summary>
            /// Wind speed (km h⁻¹).
            /// </summary>
            public double? WindSpeedKmh
            {
                get
                {
                    if (this["wind_spd_kmh"] != null)
                    {
                        if (this["wind_spd_kmh"].GetType().IsCastableTo(typeof(long)))
                        {
                            return (long)this["wind_spd_kmh"];
                        }
                    }

                    return null;
                }
            }
            /// <summary>
            /// Relative humidity (%).
            /// </summary>
            public double? RelativeHumidity
            {
                get
                {
                    if (this["rel_hum"] != null)
                    {
                        if (this["rel_hum"].GetType().IsCastableTo(typeof(long)))
                        {
                            return (long)this["rel_hum"];
                        }
                    }

                    return null;
                }
            }
        }
    }
}
