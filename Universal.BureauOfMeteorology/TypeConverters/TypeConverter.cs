﻿using System;
using Universal.Common.Data;

namespace Universal.BureauOfMeteorology
{
    /// <summary>
    /// Custom type converter for BOM types.
    /// </summary>
    public class TypeConverter : FixedWidthDefaultTypeConverter
    {
        public override object Convert(object @object, Type toType)
        {
            if (@object != null && @object.GetType() == typeof(string))
            {
                return base.Convert(((string)@object).Trim('.'), toType);
            }
            else
            {
                return base.Convert(@object, toType);
            }
        }
    }
}
