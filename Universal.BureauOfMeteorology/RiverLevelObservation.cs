﻿using System;

namespace Universal.BureauOfMeteorology
{
    public struct RiverLevelObservation
    {
        /// <summary>
        /// The observation date time.
        /// </summary>
        public DateTime DateTime { get; set; }
        /// <summary>
        /// The level in meters.
        /// </summary>
        public double Level { get; set; }

        public RiverLevelObservation(DateTime dateTime, double level)
        {
            DateTime = dateTime;
            Level = level;
        }
    }
}
