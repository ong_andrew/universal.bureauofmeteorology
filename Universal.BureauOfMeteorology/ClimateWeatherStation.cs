﻿using Universal.Common.Data;

namespace Universal.BureauOfMeteorology
{
    /// <summary>
    /// Represents a weather station that provides climate data.
    /// </summary>
    public class ClimateWeatherStation
    {
        /// <summary>
        /// The ID of the weather station.
        /// </summary>
        [FixedWidthRange(0, 5)]
        public string Id { get; set; }
        /// <summary>
        /// The state of the station.
        /// </summary>
        [FixedWidthRange(8, 10)]
        public string State { get; set; }
        /// <summary>
        /// The name of the station.
        /// </summary>
        [FixedWidthRange(18, 58)]
        public string Name { get; set; }
        /// <summary>
        /// The latitude of the station.
        /// </summary>
        [FixedWidthRange(75, 82)]
        public double Latitude { get; set; }
        /// <summary>
        /// The longitude of the station.
        /// </summary>
        [FixedWidthRange(84, 91)]
        public double Longitude { get; set; }
    }
}
