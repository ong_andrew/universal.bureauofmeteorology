﻿using System;

namespace Universal.BureauOfMeteorology
{
    /// <summary>
    /// Represents a climate data line entry in a file.
    /// </summary>
    public class ClimateData
    {
        /// <summary>
        /// The name of the weather station.
        /// </summary>
        public string StationName { get; set; }
        /// <summary>
        /// The date of the observation.
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// The evapo-transpiration (mm).
        /// </summary>
        public double? EvapoTranspiration { get; set; }
        /// <summary>
        /// The amount of rainfall (mm).
        /// </summary>
        public double? Rain { get; set; }
        /// <summary>
        /// The pan evaporation (mm).
        /// </summary>
        public double? PanEvaporation { get; set; }
        /// <summary>
        /// The maximum temperature (°C).
        /// </summary>
        public double? MaximumTemperature { get; set; }
        /// <summary>
        /// The minimum temperature (°C).
        /// </summary>
        public double? MinimumTemperature { get; set; }
        /// <summary>
        /// Maximum relative humidity (%).
        /// </summary>
        public double? MaximumRelativeHumidity { get; set; }
        /// <summary>
        /// Minimum relative humidity (%).
        /// </summary>
        public double? MinimumRelativeHumidity { get; set; }
        /// <summary>
        /// Average wind speed (m s⁻¹).
        /// </summary>
        public double? AverageWindSpeed { get; set; }
        /// <summary>
        /// Solar radiation (MJ m⁻²).
        /// </summary>
        public double? SolarRadiation { get; set; }
    }
}
