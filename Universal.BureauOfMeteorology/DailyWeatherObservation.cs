﻿using System;
using Universal.Common;

namespace Universal.BureauOfMeteorology
{
    /// <summary>
    /// Represents a daily weather observation.
    /// </summary>
    public class DailyWeatherObservation
    {
		/// <summary>
		/// The date of the observation.
		/// </summary>
		public Date Date { get; set; }
		/// <summary>
		/// Minimum temperature (°C).
		/// </summary>
		public decimal? MinimumTemperature { get; set; }
		/// <summary>
		/// Maximum temperature (°C).
		/// </summary>
		public decimal? MaximumTemperature { get; set; }
		/// <summary>
		/// Rainfall (mm)
		/// </summary>
		public decimal? Rainfall { get; set; }
		/// <summary>
		/// Evaporation (mm)
		/// </summary>
		public decimal? Evaporation { get; set; }
		/// <summary>
		/// Sunshine (hours)
		/// </summary>
		public decimal? Sunshine { get; set; }
		/// <summary>
		/// Direction of maximum wind gust 
		/// </summary>
		public string DirectionOfMaximumWindGust { get; set; }
		/// <summary>
		/// Speed of maximum wind gust (km/h)
		/// </summary>
		public decimal? SpeedOfMaximumWindGust { get; set; }
		/// <summary>
		/// Time of maximum wind gust
		/// </summary>
		public TimeSpan? TimeOfMaximumWindGust { get; set; }
		/// <summary>
		/// 9am relative humidity (%)
		/// </summary>
		public decimal? Temperature9AM { get; set; }
		/// <summary>
		/// 9am relative humidity (%)
		/// </summary>
		public decimal? RelativeHumidity9AM { get; set; }
		/// <summary>
		/// 9am cloud amount (oktas)
		/// </summary>
		public decimal? CloudAmount9AM { get; set; }
		/// <summary>
		/// 9am wind direction
		/// </summary>
		public string WindDirection9AM { get; set; }
		/// <summary>
		/// 9am wind speed (km/h)
		/// </summary>
		public decimal? WindSpeed9AM { get; set; }
		/// <summary>
		/// 9am MSL pressure (hPa)
		/// </summary>
		public decimal? MeanSeaLevelPressure9AM { get; set; }
		/// <summary>
		/// 3pm Temperature (°C)
		/// </summary>
		public decimal? Temperature3PM { get; set; }
		/// <summary>
		/// 3pm relative humidity (%)
		/// </summary>
		public decimal? RelativeHumidity3PM { get; set; }
		/// <summary>
		/// 3pm cloud amount (oktas)
		/// </summary>
		public decimal? CloudAmount3PM { get; set; }
		/// <summary>
		/// 3pm wind direction
		/// </summary>
		public string WindDirection3PM { get; set; }
		/// <summary>
		/// 3pm wind speed (km/h)
		/// </summary>
		public decimal? WindSpeed3PM { get; set; }
		/// <summary>
		/// 3pm MSL pressure (hPa)
		/// </summary>
		public decimal? MeanSeaLevelPressure3PM { get; set; }
	}
}
