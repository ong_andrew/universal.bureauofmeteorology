﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common;
using Universal.Common.Data;
using Universal.Common.Net.Ftp;
using Universal.Common.Net.Http;
using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.BureauOfMeteorology
{
    /// <summary>
    /// Class for interacting with Bureau of Meteorology data sources.
    /// </summary>
    public class BureauOfMeteorologyClient : IDisposable
    {
        private static Dictionary<string, (string State, string StationName)> StationIdStateNameMapping = new Dictionary<string, (string State, string StationName)>()
        {
            ["009215"] = ("wa", "swanbourne"),
            ["009225"] = ("wa", "perth_metro"),
            ["009021"] = ("wa", "perth_airport"),
            ["009281"] = ("wa", "millendon_(swan_valley)"),
            ["009172"] = ("wa", "jandakot_aero"),
            ["086038"] = ("vic", "essendon_airport"),
            ["086282"] = ("vic", "melbourne_airport"),
            ["086068"] = ("vic", "viewbank"),
            ["086338"] = ("vic", "melbourne_(olympic_park)"),
            ["088162"] = ("vic", "wallan_(kilmore_gap)"),
            ["090186"] = ("vic", "warrnambool_airport_ndb"),
            ["090175"] = ("vic", "port_fairy"),
            ["090176"] = ("vic", "mortlake_racecourse"),
            ["089112"] = ("vic", "westmere"),
            ["090184"] = ("vic", "cape_nelson_lighthouse")
        };

        private FtpClient FtpClient { get; set; }
        private HttpServiceClient HttpServiceClient { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BureauOfMeteorologyClient"/>.
        /// </summary>
        public BureauOfMeteorologyClient()
        {
            FtpClient = new FtpClient();
            HttpServiceClient = new HttpServiceClient();
        }

        /// <summary>
        /// Downloads the daily weather observations file.
        /// </summary>
        /// <param name="dailyWeatherObservationId"></param>
        /// <param name="month"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Stream> DownloadDailyWeatherObservationsFileAsync(string dailyWeatherObservationId, Date month, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await HttpServiceClient.ExecuteAsync(HttpMethod.Get, new Uri($"http://www.bom.gov.au/climate/dwo/{month:yyyyMM}/text/IDCJDW{dailyWeatherObservationId}.{month:yyyyMM}.csv"), httpRequestMessage =>
            {
                httpRequestMessage.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                httpRequestMessage.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36");
            }, cancellationToken).ConfigureAwait(false))
            {
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    MemoryStream memoryStream = new MemoryStream();
                    await (await httpResponseMessage.Content.ReadAsStreamAsync().ConfigureAwait(false)).CopyToAsync(memoryStream);
                    memoryStream.Position = 0;
                    return memoryStream;
                }
                else
                {
                    Console.WriteLine(httpResponseMessage.StatusCode);
                    Console.WriteLine(httpResponseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult());
                    throw new FileNotFoundException("The specified daily weather observation file was not found.");
                }
            }
        }

        /// <summary>
        /// Asynchronously downloads the climate data file for the given state, station, and month.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="stationName"></param>
        /// <param name="month"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Stream> DownloadClimateDataFileAsync(string state, string stationName, DateTime month, CancellationToken cancellationToken = default)
        {
            string fileName = $"{stationName}-{month:yyyyMM}.csv";

            UriBuilder uriBuilder =
                new UriBuilder("ftp://ftp.bom.gov.au/anon/gen/clim_data/IDCKWCDEA0/tables")
                    .AddSegment(state)
                    .AddSegment(stationName)
                    .AddSegment(fileName);

            return await FtpClient.DownloadFileAsync(uriBuilder).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets a list of weather stations providing climate data.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ClimateWeatherStation>> GetClimateWeatherStationsAsync(CancellationToken cancellationToken = default)
        {
            List<ClimateWeatherStation> result = new List<ClimateWeatherStation>();
            FixedWidthFileFormat fixedWidthFileFormat = FixedWidthFileFormat.FromType<ClimateWeatherStation>();
            using (Stream climateStationDataStream = await FtpClient.DownloadFileAsync(new Uri("ftp://ftp.bom.gov.au/anon/gen/clim_data/IDCKWCDEA0/tables/stations_db.txt")).ConfigureAwait(false))
            using (FixedWidthFileReader fixedWidthFileReader = new FixedWidthFileReader(climateStationDataStream, fixedWidthFileFormat) { Trim = true })
            {
                FixedWithFileRangeDataMapper<ClimateWeatherStation> fixedWithFileRangeDataMapper = new FixedWithFileRangeDataMapper<ClimateWeatherStation>();
                fixedWithFileRangeDataMapper.ConfigureRanges(fixedWidthFileFormat.FieldRanges);

                while (fixedWidthFileReader.Read())
                {
                    result.Add(fixedWithFileRangeDataMapper.Map(fixedWidthFileReader.GetLine()));
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the climate data for the given station by month.
        /// </summary>
        /// <param name="stationId"></param>
        /// <param name="month"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ClimateData>> GetClimateDataAsync(string stationId, DateTime month, CancellationToken cancellationToken = default)
        {
            string state, stationName;
            if (StationIdStateNameMapping.ContainsKey(stationId))
            {
                (state, stationName) = StationIdStateNameMapping[stationId];
            }
            else
            {
                (state, stationName) = await TryMapClimateStationIdToStateNameAsync(stationId).ConfigureAwait(false);
            }

            if (state != null && stationName != null)
            {
                return await GetClimateDataAsync(state, stationName, month, cancellationToken).ConfigureAwait(false);
            }
            else
            {
                throw new NotSupportedException($"No mapping from provided station ID {stationId} to a state and station name. Consider using GetClimateDataAsync(string state, string stationName, DateTime month) instead.");
            }
        }

        /// <summary>
        /// Gets the climate data for the given state, station, and month.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="stationName"></param>
        /// <param name="month"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ClimateData>> GetClimateDataAsync(string state, string stationName, DateTime month, CancellationToken cancellationToken = default)
        {
            List<ClimateData> result = new List<ClimateData>();

            using (Stream climateDataStream = await DownloadClimateDataFileAsync(state, stationName, month, cancellationToken).ConfigureAwait(false))
            using (MemoryStream memoryStream = new MemoryStream())
            using (StreamReader streamReader = new StreamReader(climateDataStream))
            using (StreamWriter streamWriter = new StreamWriter(memoryStream))
            {
                for (int i = 0; i < 13; i++)
                {
                    streamReader.ReadLine();
                }

                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();

                    if (line.StartsWith("Totals:"))
                    {
                        break;
                    }

                    streamWriter.WriteLine(line);
                }

                streamWriter.Flush();

                memoryStream.Position = 0;

                DelimitedFileFormat delimitedFileFormat = new DelimitedFileFormat(DelimitedFileFormat.CommaSeparatedValues)
                {
                    HasHeader = false
                };

                using (DelimitedFileReader delimitedFileReader = new DelimitedFileReader(memoryStream, delimitedFileFormat))
                {
                    while (delimitedFileReader.Read())
                    {
                        ClimateData climateData = new ClimateData();
                        climateData.StationName = delimitedFileReader[0];
                        climateData.Date = DateTime.ParseExact(delimitedFileReader[1], "dd/MM/yyyy", CultureInfo.CurrentCulture);
                        if (double.TryParse(delimitedFileReader[2], out double evapoTranspiration))
                        {
                            climateData.EvapoTranspiration = evapoTranspiration;
                        }
                        if (double.TryParse(delimitedFileReader[3], out double rain))
                        {
                            climateData.Rain = rain;
                        }
                        if (double.TryParse(delimitedFileReader[4], out double panEvaporation))
                        {
                            climateData.PanEvaporation = panEvaporation;
                        }
                        if (double.TryParse(delimitedFileReader[5], out double maximumTemperature))
                        {
                            climateData.MaximumTemperature = maximumTemperature;
                        }
                        if (double.TryParse(delimitedFileReader[6], out double minimumTemperature))
                        {
                            climateData.MinimumTemperature = minimumTemperature;
                        }
                        if (double.TryParse(delimitedFileReader[7], out double maximumHumidity))
                        {
                            climateData.MaximumRelativeHumidity = maximumHumidity;
                        }
                        if (double.TryParse(delimitedFileReader[8], out double minimumHumidity))
                        {
                            climateData.MinimumRelativeHumidity = minimumHumidity;
                        }
                        if (double.TryParse(delimitedFileReader[9], out double averageWindSpeed))
                        {
                            climateData.AverageWindSpeed = averageWindSpeed;
                        }
                        if (double.TryParse(delimitedFileReader[10], out double solarRadiation))
                        {
                            climateData.SolarRadiation = solarRadiation;
                        }

                        result.Add(climateData);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the daily weather observations for the specified ID and month.
        /// </summary>
        /// <param name="dailyWeatherObservationId"></param>
        /// <param name="month"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<DailyWeatherObservation>> GetDailyWeatherObservationsAsync(string dailyWeatherObservationId, Date month, CancellationToken cancellationToken = default)
        {
            using (Stream stream = await DownloadDailyWeatherObservationsFileAsync(dailyWeatherObservationId, month, cancellationToken).ConfigureAwait(false))
            using (MemoryStream memoryStream = new MemoryStream())
            using (StreamReader streamReader = new StreamReader(stream))
            using (StreamWriter streamWriter = new StreamWriter(memoryStream))
            {
                bool foundHeader = false;

                while (!foundHeader)
                {
                    string line = streamReader.ReadLine();

                    if (line != null)
                    {
                        if (line.StartsWith(",\"Date\""))
                        {
                            foundHeader = true;
                            streamWriter.WriteLine(line);
                            break;
                        }
                    }
                    else
                    {
                        throw new FormatException("Null line read from stream.");
                    }
                }

                if (!foundHeader)
                {
                    throw new FormatException("File was not in correct format. Cannot find header row.");
                }

                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    streamWriter.WriteLine(line);
                }

                streamWriter.Flush();

                memoryStream.Position = 0;

                List<DailyWeatherObservation> result = new List<DailyWeatherObservation>();

                using (DelimitedFileReader delimitedFileReader = new DelimitedFileReader(memoryStream, DelimitedFileFormat.CommaSeparatedValues))
                {
                    while (delimitedFileReader.Read())
                    {
                        DailyWeatherObservation dailyWeatherObservation = new DailyWeatherObservation()
                        {
                            Date = delimitedFileReader.Get<Date>("Date"),
                            MinimumTemperature = delimitedFileReader.Get<decimal?>(2),
                            MaximumTemperature = delimitedFileReader.Get<decimal?>(3),
                            Rainfall = delimitedFileReader.Get<decimal?>(4),
                            Evaporation = delimitedFileReader.Get<decimal?>(5),
                            Sunshine = delimitedFileReader.Get<decimal?>(6),
                            DirectionOfMaximumWindGust = delimitedFileReader.Get<string>(7),
                            SpeedOfMaximumWindGust = delimitedFileReader.Get<decimal?>(8),
                            TimeOfMaximumWindGust = delimitedFileReader.Get<TimeSpan?>(9),
                            Temperature9AM = delimitedFileReader.Get<decimal?>(10),
                            RelativeHumidity9AM = delimitedFileReader.Get<decimal?>(11),
                            CloudAmount9AM = delimitedFileReader.Get<decimal?>(12),
                            WindDirection9AM = delimitedFileReader.Get<string>(13),
                            WindSpeed9AM = delimitedFileReader.Get<decimal?>(14),
                            MeanSeaLevelPressure9AM = delimitedFileReader.Get<decimal?>(15),
                            Temperature3PM = delimitedFileReader.Get<decimal?>(16),
                            RelativeHumidity3PM = delimitedFileReader.Get<decimal?>(17),
                            CloudAmount3PM = delimitedFileReader.Get<decimal?>(18),
                            WindDirection3PM = delimitedFileReader.Get<string>(19),
                            WindSpeed3PM = delimitedFileReader.Get<decimal?>(20),
                            MeanSeaLevelPressure3PM = delimitedFileReader.Get<decimal?>(21),
                        };

                        result.Add(dailyWeatherObservation);
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the daily weather observations for the specified station ID and month.
        /// </summary>
        /// <param name="stationId"></param>
        /// <param name="month"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<DailyWeatherObservation>> GetDailyWeatherObservationsByStationIdAsync(string stationId, Date month, CancellationToken cancellationToken = default)
        {
            return await GetDailyWeatherObservationsAsync(StationIdMapper.GetDailyWeatherObservationId(stationId), month, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets the latest weather observations (ID(State)60801) for the given weather station.
        /// </summary>
        /// <param name="stationId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<LatestWeatherObservationsResponse> GetLatestWeatherObservationsByStationIdAsync(string stationId, CancellationToken cancellationToken = default)
        {
            WeatherStation weatherStation = (await GetWeatherStationsAsync(cancellationToken).ConfigureAwait(false)).FirstOrDefault(x => x.Site == stationId);
            
            if (weatherStation != null)
            {
                if (!weatherStation.WmoReference.IsNullOrEmpty())
                {
                    return await GetLatestWeatherObservationsByWmoReferenceAsync(weatherStation.State, weatherStation.WmoReference, cancellationToken).ConfigureAwait(false);
                }
                else
                {
                    throw new InvalidOperationException($"The weather station {stationId} does not have a corresponding WMO Reference.");
                }
            }
            else
            {
                throw new InvalidOperationException($"No weather station with ID {stationId} exists.");
            }
        }

        /// <summary>
        /// Gets the latest weather observations for the given WMO reference.
        /// </summary>
        /// <param name="state"></param>
        /// <param name="wmoReference"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<LatestWeatherObservationsResponse> GetLatestWeatherObservationsByWmoReferenceAsync(string state, string wmoReference, CancellationToken cancellationToken = default)
        {
            if (state == null)
            {
                throw new ArgumentNullException(nameof(state));
            }
            else if (state.Length < 1)
            {
                throw new ArgumentException(nameof(state));
            }

            char stateCharacter = state[0].ToUpperInvariant();

            UriBuilder uriBuilder = new UriBuilder($"http://www.bom.gov.au/fwo/ID{stateCharacter}60801/ID{stateCharacter}60801.{wmoReference}.json");
            
            using (HttpResponseMessage httpResponseMessage = await HttpServiceClient.ExecuteAsync(HttpMethod.Get, uriBuilder, httpRequestMessage => 
            {
                httpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(MediaTypes.Application.Json));
                httpRequestMessage.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36");
            }, cancellationToken).ConfigureAwait(false))
            {
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    return LatestWeatherObservationsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
                }
                else
                {
                    throw new HttpException(httpResponseMessage);
                }
            }
        }

        /// <summary>
        /// Gets an enumeration of all BoM weather stations from the FTP file.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IEnumerable<WeatherStation>> GetWeatherStationsAsync(CancellationToken cancellationToken = default)
        {
            Stream stationsZipStream = await DownloadWeatherStationsFileAsync(cancellationToken).ConfigureAwait(false);
            ZipArchive stationsZipArchive = new ZipArchive(stationsZipStream);
            ZipArchiveEntry stationsZipArchiveEntry = stationsZipArchive.GetEntry("stations.txt");

            FixedWidthFileFormat fixedWidthFileFormat = FixedWidthFileFormat.FromType<WeatherStation>();
            FixedWithFileRangeDataMapper<WeatherStation> mapper = new FixedWithFileRangeDataMapper<WeatherStation>(new TypeConverter());
            mapper.ConfigureRanges(fixedWidthFileFormat.FieldRanges);
            List<WeatherStation> result = new List<WeatherStation>();

            using (MemoryStream memoryStream = new MemoryStream())
            using (StreamReader streamReader = new StreamReader(stationsZipArchiveEntry.Open()))
            using (StreamWriter streamWriter = new StreamWriter(memoryStream))
            {
                for (int i = 0; i < 4; i++)
                {
                    streamReader.ReadLine();
                }

                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();

                    if (line.Length != 135)
                    {
                        break;
                    }

                    streamWriter.WriteLine(line);
                }

                streamWriter.Flush();

                memoryStream.Position = 0;

                using (FixedWidthFileReader fixedWidthFileReader = new FixedWidthFileReader(memoryStream, fixedWidthFileFormat) { Trim = true })
                {
                    while (fixedWidthFileReader.Read())
                    {
                        WeatherStation weatherStation = mapper.Map(fixedWidthFileReader.GetLine());
                        result.Add(weatherStation);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Downloads the weather stations zip file at ftp://ftp.bom.gov.au/anon2/home/ncc/metadata/sitelists/stations.zip.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Stream> DownloadWeatherStationsFileAsync(CancellationToken cancellationToken = default)
        {
            return await FtpClient.DownloadFileAsync(new Uri("ftp://ftp.bom.gov.au/anon2/home/ncc/metadata/sitelists/stations.zip")).ConfigureAwait(false);
        }

        /// <summary>
        /// Attempts to map a station ID to a climate station state and name.
        /// </summary>
        /// <param name="stationId"></param>
        /// <returns></returns>
        protected virtual async Task<(string State, string StationName)> TryMapClimateStationIdToStateNameAsync(string stationId)
        {
            ClimateWeatherStation climateWeatherStation = (await GetClimateWeatherStationsAsync().ConfigureAwait(false)).FirstOrDefault(x => x.Id == stationId);

            if (climateWeatherStation != null)
            {
                string state = climateWeatherStation.State.ToLowerInvariant();
                UriBuilder uriBuilder = new UriBuilder("ftp://ftp.bom.gov.au/anon/gen/clim_data/IDCKWCDEA0/tables").AddSegment(state);
                IEnumerable<DirectoryItem> directoryItems = await FtpClient.ListDirectoryItemsAsync(uriBuilder).ConfigureAwait(false);

                string convertedName = climateWeatherStation.Name.ToLowerInvariant().Replace(" ", "_");
                if (directoryItems.Any(x => x.IsDirectory && x.Name == convertedName))
                {
                    return (state, convertedName);
                }

                convertedName = convertedName.Replace("_aws_", "_");
                if (convertedName.EndsWith("_aws"))
                {
                    convertedName = convertedName.Substring(0, convertedName.Length - 4);
                }
                if (directoryItems.Any(x => x.IsDirectory && x.Name == convertedName))
                {
                    return (state, convertedName);
                }
            }

            return (null, null);
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    FtpClient.Dispose();
                    HttpServiceClient.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

        /*
        public async Task<IEnumerable<RiverLevelObservation>> GetRiverLevelAsync(string bomStationId)
        {
            using (HttpServiceClient httpServiceClient = new HttpServiceClient())
            {
                using (HttpResponseMessage httpResponseMessage = await httpServiceClient.ExecuteAsync(HttpMethod.Get, new Uri($"http://www.bom.gov.au/fwo/IDQ65393/IDQ65393.{bomStationId}.tbl.shtml"), httpRequestMessageModifier: (httpRequestMessage) =>
                {
                    httpRequestMessage.Headers.UserAgent.ParseAdd("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.56 Safari/537.36");
                }).ConfigureAwait(false))
                {
                    HtmlDocument htmlDocument = new HtmlDocument();
                    htmlDocument.LoadHtml(await httpResponseMessage.Content.ReadAsStringAsync());
                    HtmlNode tableBodyNode = htmlDocument.DocumentNode.QuerySelector(".tabledata tbody");

                    List<RiverLevelObservation> result = new List<RiverLevelObservation>();
                    foreach (HtmlNode dataRowNode in tableBodyNode.QuerySelectorAll("tr").ToList())
                    {
                        DateTime dateTime = DateTime.ParseExact(dataRowNode.QuerySelectorAll("td").ElementAt(0).InnerHtml, "dd/MM/yyyy HH:mm", Thread.CurrentThread.CurrentCulture);
                        double level = double.Parse(dataRowNode.QuerySelectorAll("td").ElementAt(1).InnerHtml);

                        result.Add(new RiverLevelObservation(dateTime, level));
                    }

                    return result;
                }
            }
        }
        */
    }
}
