﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;

namespace Universal.BureauOfMeteorology
{
    [TestClass]
    [TestCategory("LatestWeatherObservationsResponse")]
    public class LatestWeatherObservationsResponseTests
    {
        [TestMethod]
        public void CanDeserialize()
        {
            LatestWeatherObservationsResponse response = LatestWeatherObservationsResponse.FromString(File.ReadAllText("Resources/Sample IDV60801 Response.json"));
            Assert.IsTrue(response.Observations.Data.All(x => x.Name == "Cerberus"));
            Assert.IsTrue(response.Observations.Data.All(x => x.LocalDateTime != default));
            Assert.IsTrue(response.Observations.Data.Any(x => x.RainTrace == 0.0));
            Assert.IsTrue(response.Observations.Data.Any(x => x.RainTrace > 0));
            Assert.IsTrue(response.Observations.Data.Any(x => x.RainTrace > 0));
            Assert.IsTrue(response.Observations.Data.All(x => x.AirTemperature != null));
            Assert.IsTrue(response.Observations.Data.All(x => x.RelativeHumidity > 0));
            Assert.IsTrue(response.Observations.Data.All(x => x.WindSpeedKmh > 0));
        }
    }
}
