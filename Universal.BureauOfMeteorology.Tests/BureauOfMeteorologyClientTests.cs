using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Universal.Common;

namespace Universal.BureauOfMeteorology
{
    [TestClass]
    [TestCategory("BureauOfMeteorologyClient")]
    public class BureauOfMeteorologyClientTests
    {
        [TestMethod]
        public async Task CanGetAllWeatherStations()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                List<WeatherStation> weatherStations = (await client.GetWeatherStationsAsync()).ToList();
                Assert.AreNotEqual(0, weatherStations.Count);
            }
        }

        [TestMethod]
        public async Task CanGetClimateWeatherStations()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                IEnumerable<ClimateWeatherStation> weatherStations = (await client.GetClimateWeatherStationsAsync()).ToList();
                Assert.AreNotEqual(0, weatherStations.Count());
            }
        }

        [TestMethod]
        public async Task CanDownloadClimateDataFile()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                Stream stream = await client.DownloadClimateDataFileAsync("nsw", "albion_park_(shellharbour_airport)", new DateTime(2020, 01, 01));
                Assert.AreNotEqual(0, stream.Length);
            }
        }

        [TestMethod]
        public async Task CanGetClimateData()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                IEnumerable<ClimateData> climateData = await client.GetClimateDataAsync("nsw", "albion_park_(shellharbour_airport)", new DateTime(2020, 01, 01));
                Assert.AreNotEqual(0, climateData.Count());
            }
        }

        [TestMethod]
        public async Task CanGetClimateDataById()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                IEnumerable<ClimateData> climateData = await client.GetClimateDataAsync("068242", new DateTime(2020, 01, 01));
                Assert.AreNotEqual(0, climateData.Count());

                climateData = await client.GetClimateDataAsync("066212", new DateTime(2020, 01, 01));
                Assert.AreNotEqual(0, climateData.Count());

                climateData = await client.GetClimateDataAsync("066194", new DateTime(2020, 01, 01));
                Assert.AreNotEqual(0, climateData.Count());

                climateData = await client.GetClimateDataAsync("066137", new DateTime(2020, 01, 01));
                Assert.AreNotEqual(0, climateData.Count());

                climateData = await client.GetClimateDataAsync("009215", new DateTime(2020, 01, 01));
                Assert.AreNotEqual(0, climateData.Count());

                climateData = await client.GetClimateDataAsync("009225", new DateTime(2020, 08, 01));
                Assert.AreNotEqual(0, climateData.Count());
            }
        }

        [TestMethod]
        public async Task CanGetLatestObservations()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                var response = await client.GetLatestWeatherObservationsByWmoReferenceAsync("VIC", "94898");
                Assert.AreNotEqual(0, response.Count());
                Assert.IsTrue(response.All(x => x.Name == "Cerberus"));
                Assert.AreEqual("IDV60801", response.Observations.Header.First().Id);
            }
        }

        [TestMethod]
        public async Task CanGetLatestObservationsForNSW()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                var response = await client.GetLatestWeatherObservationsByWmoReferenceAsync("NSW", "95753");
                Assert.AreNotEqual(0, response.Count());
                Assert.IsTrue(response.All(x => x.Name == "Richmond"));
                Assert.AreEqual("IDN60801", response.Observations.Header.First().Id);
            }
        }

        [TestMethod]
        public async Task CanGetLatestObservationsForWA()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                var response = await client.GetLatestWeatherObservationsByStationIdAsync("009215");
                Assert.AreNotEqual(0, response.Count());
                Assert.IsTrue(response.All(x => x.Name == "Swanbourne"));
                Assert.AreEqual("IDW60801", response.Observations.Header.First().Id);

                response = await client.GetLatestWeatherObservationsByStationIdAsync("009225");
                Assert.AreNotEqual(0, response.Count());
                Assert.IsTrue(response.All(x => x.Name == "Perth"));
                Assert.AreEqual("IDW60801", response.Observations.Header.First().Id);

                response = await client.GetLatestWeatherObservationsByWmoReferenceAsync("WA", "94610");
                Assert.AreNotEqual(0, response.Count());
                Assert.IsTrue(response.All(x => x.Name == "Perth Airport"));
                Assert.AreEqual("IDW60801", response.Observations.Header.First().Id);
            }
        }

        [TestMethod]
        public async Task CanGetLatestObservationsByStationId()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                var response = await client.GetLatestWeatherObservationsByStationIdAsync("076064");
                Assert.AreNotEqual(0, response.Count());
                Assert.IsTrue(response.All(x => x.Name == "Walpeup"));
                Assert.AreEqual("IDV60801", response.Observations.Header.First().Id);
            }
        }

        [TestMethod]
        public async Task CanEnumerateDirectly()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                IEnumerable<LatestWeatherObservationsResponse.WeatherObservation> latestWeatherObservations = await client.GetLatestWeatherObservationsByStationIdAsync("090186");
                Assert.AreNotEqual(0, latestWeatherObservations.Count());
            }
        }

        [TestMethod]
        public async Task CanDownloadDailyWeatherObservationsFile()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                Stream stream = await client.DownloadDailyWeatherObservationsFileAsync("3033", Date.Today).ConfigureAwait(false);

                Assert.IsNotNull(stream);
            }
        }

        [TestMethod]
        public async Task CanGrabDailyWeatherObservations()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                IEnumerable<DailyWeatherObservation> dailyWeatherObservations = await client.GetDailyWeatherObservationsAsync("3033", Date.Today).ConfigureAwait(false);

                Assert.AreNotEqual(0, dailyWeatherObservations.Count());

                Console.WriteLine(JsonConvert.SerializeObject(dailyWeatherObservations));
            }
        }

        [TestMethod]
        public async Task CanGrabDailyWeatherObservationsFor2097()
        {
            using (BureauOfMeteorologyClient client = new BureauOfMeteorologyClient())
            {
                IEnumerable<DailyWeatherObservation> dailyWeatherObservations = await client.GetDailyWeatherObservationsAsync("2097", new Date(2021, 01, 06)).ConfigureAwait(false);

                Assert.AreNotEqual(0, dailyWeatherObservations.Count());

                Console.WriteLine(JsonConvert.SerializeObject(dailyWeatherObservations));
            }
        }
    }
}
