﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Universal.BureauOfMeteorology
{
    [TestClass]
    [TestCategory("StationIdMapper")]
    public class StationIdMapperTests
    {
        [TestMethod]
        public void CanMap()
        {
            Assert.IsNull(StationIdMapper.GetStateWmoReference("ASDF"));
            Assert.IsNull(StationIdMapper.GetDailyWeatherObservationId("ASDF"));
            Assert.IsNotNull(StationIdMapper.GetStateWmoReference("300017"));
            Assert.IsNotNull(StationIdMapper.GetDailyWeatherObservationId("300017"));
        }

        [TestMethod]
        public void CanMapStations()
        {
            // Ararat, VIC
            Assert.AreEqual("3002", StationIdMapper.GetDailyWeatherObservationId("089085"));
            // Melbourn Olympic Park, VIC
            Assert.AreEqual("3033", StationIdMapper.GetDailyWeatherObservationId("086338"));
            // Alice Springs, NT
            Assert.AreEqual("8002", StationIdMapper.GetDailyWeatherObservationId("015590"));
        }

        [TestMethod]
        public void CanBuildProjections()
        {
            StationIdMapper.GetStationIdStateWmoReferenceMap();
            StationIdMapper.GetStationIdDailyWeatherObservationIdMap();
        }
    }
}
