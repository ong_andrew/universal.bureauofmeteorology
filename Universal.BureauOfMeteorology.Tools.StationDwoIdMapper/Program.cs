﻿using System.IO;
using System.Threading.Tasks;
using Universal.CodeGeneration.CSharp;

namespace Universal.BureauOfMeteorology.Tools.StationDwoIdMapper
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            using (BureauOfMeteorologyClient bureauOfMeteorologyClient = new BureauOfMeteorologyClient())
            {
                File.WriteAllText("StationIdDailyWeatherObservationIdMap.csdict", (await StationIdMapper.DownloadStationIdDailyWeatherObservationIdMapAsync().ConfigureAwait(false)).ToInitializerString());
            }
        }
    }
}
