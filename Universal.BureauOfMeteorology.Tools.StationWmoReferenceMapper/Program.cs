﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Universal.CodeGeneration.CSharp;
using Universal.Common;

namespace Universal.BureauOfMeteorology.Tools.StationWmoReferenceMapper
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            using (BureauOfMeteorologyClient bureauOfMeteorologyClient = new BureauOfMeteorologyClient())
            {
                Dictionary<string, (string State, string WmoReference)> mapping = new Dictionary<string, (string State, string WmoReference)>();
                foreach (var weatherStation in await bureauOfMeteorologyClient.GetWeatherStationsAsync())
                {
                    if (!weatherStation.WmoReference.IsNullOrEmpty())
                    {
                        mapping.Add(weatherStation.Site, (weatherStation.State, weatherStation.WmoReference));
                    }
                }

                File.WriteAllText("StationIdStateWmoReferenceMap.csdict", mapping.ToInitializerString());
            }
        }
    }
}
